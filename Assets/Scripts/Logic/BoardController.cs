﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Utility;
using UnityEngine;

namespace Assets.Scripts.Logic
{
    public class BoardController
    {
        private readonly IBoardBehaviour boardBehaviour;
        private readonly IAttackManager attackManager;
        private readonly int gridSize;

        public BoardController(IBoardBehaviour boardBehaviour, IAttackManager attackManager, int gridSize)
        {
            this.boardBehaviour = boardBehaviour;
            this.attackManager = attackManager;
            this.gridSize = gridSize;

            SetupArrays();
            PurpleTeamTurn = true;

            AgentActionOptions = new AgentOptions(gridSize);
        }

        private void SetupArrays()
        {
            Agents = new IAgent[gridSize, gridSize];

            ImpassableTerrain = new bool[gridSize, gridSize];
            PurpleBase = new bool[gridSize, gridSize];
            OrangeBase = new bool[gridSize, gridSize];
        }

        public bool PurpleTeamTurn { get; set; }

        public IAgent[,] Agents { get; set; }

        public bool[,] ImpassableTerrain { get; set; }

        public bool[,] PurpleBase { get; set; }

        public bool[,] OrangeBase { get; set; }

        public Point? FlagLocation { get; set; }

        public IAgent SelectedAgent { get; set; }

        public ActionType SelectedAction { get; set; }

        public AgentOptions AgentActionOptions { get; set; }

        public void BoardClicked(int x, int y)
        {
            if (x >= 0 && y >= 0)
            {
                if (SelectedAgent == null)
                {
                    SelectAgent(x, y);
                }
                else if (SelectedAction == ActionType.Shoot && AgentActionOptions.ValidRanged[x, y])
                {
                    attackManager.MakeRangedAttack(SelectedAgent, Agents[x, y]);
                    boardBehaviour.PlaySelectionSound();
                    SelectedAgent = null;
                }
                else if (SelectedAction == ActionType.Melee && AgentActionOptions.ValidMelee[x, y])
                {
                    attackManager.MakeMeleeAttacks(SelectedAgent, Agents[x, y]);
                    boardBehaviour.PlaySelectionSound();
                    SelectedAgent = null;
                }
                else if (SelectedAction == ActionType.Move && AgentActionOptions.PossibleMoves[x, y])
                {
                    MoveAgent(x, y);
                    boardBehaviour.PlaySelectionSound();
                }
            }
        }

        private void SelectAgent(int x, int y)
        {
            IAgent agentAtSelection = Agents[x, y];

            if (agentAtSelection != null)
            {
                SelectedAgent = agentAtSelection;
                boardBehaviour.ShowSelectedAgent(SelectedAgent.GetAgentStats());
                boardBehaviour.PlaySelectionSound();
                UpdateAgentOptions();
            }
        }

        private void UpdateAgentOptions()
        {
            if (SelectedAgent.PurpleTeam == PurpleTeamTurn)
            {
                AgentActionOptions = SelectedAgent.GetActionOptions(Agents, ImpassableTerrain);
            }
            else
            {
                AgentActionOptions = new AgentOptions(gridSize);
            }
        }

        private void MoveAgent(int x, int y)
        {
            Agents[SelectedAgent.CurrentX, SelectedAgent.CurrentY] = null;
            SelectedAgent.MoveTo(x, y);
            Agents[x, y] = SelectedAgent;

            if (MovedOntoFlag(x, y))
            {
                SelectedAgent.PickupFlag();
                boardBehaviour.FlagPickedUp();
                boardBehaviour.ClearLog();
                boardBehaviour.AddToLog(string.Format("{0} collected flag", SelectedAgent.Name));
                boardBehaviour.ShowFlagHolder(SelectedAgent.Name);
            }

            UpdateAgentOptions();
        }

        private bool MovedOntoFlag(int x, int y)
        {
            return FlagLocation.HasValue && FlagLocation.Value.x == x && FlagLocation.Value.y == y;
        }

        public void AgentDowned(int x, int y)
        {
            if (Agents[x, y].HoldingFlag)
            {
                Agents[x, y].DropFlag();
                boardBehaviour.SpawnFlag(x, y);
            }

            Agents[x, y].Down();
            Agents[x, y] = null;
        }

        public void CheckForRoundEnd()
        {
            var orangeTeamEliminated = true;
            var purpleTeamEliminated = true;

            var purpleTeamCapture = false;
            var orangeTeamCapture = false;

            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    var agent = Agents[i, j];

                    if (agent != null)
                    {
                        if (!agent.PurpleTeam)
                        {
                            orangeTeamEliminated = false;

                            if (agent.HoldingFlag && OrangeBase[i, j])
                            {
                                orangeTeamCapture = true;
                            }
                        }
                        else if (agent.PurpleTeam)
                        {
                            purpleTeamEliminated = false;

                            if (agent.HoldingFlag && PurpleBase[i, j])
                            {
                                purpleTeamCapture = true;
                            }
                        }
                    }
                }
            }

            if (orangeTeamCapture)
            {
                boardBehaviour.EndRound(RoundOutcome.OrangeByCapture);
            }
            else if (purpleTeamCapture)
            {
                boardBehaviour.EndRound(RoundOutcome.PurpleByCapture);
            }
            else if (orangeTeamEliminated)
            {
                boardBehaviour.EndRound(RoundOutcome.PurpleByElimination);
            }
            else if (purpleTeamEliminated)
            {
                boardBehaviour.EndRound(RoundOutcome.OrangeByElimination);
            }
        }

        public void EndTurn()
        {
            SelectedAgent = null;
            PurpleTeamTurn = !PurpleTeamTurn;
            boardBehaviour.RefreshAgentMovements();

            if (PurpleTeamTurn)
            {
                boardBehaviour.ColourEndTurnPurple();
            }
            else
            {
                boardBehaviour.ColourEndTurnOrange();
            }
        }
    }
}
