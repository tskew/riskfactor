﻿using Assets.Scripts.Interfaces;

namespace Assets.Scripts.Logic
{
    public class AttackManager : IAttackManager
    {
        private readonly IBoardBehaviour board;

        public AttackManager(IBoardBehaviour board)
        {
            this.board = board;
        }

        public void MakeMeleeAttacks(IAgent attacker, IAgent defender)
        {
            board.ClearLog();
            board.AddToLog(string.Format("{0} attacks {1}", attacker.Name, defender.Name));

            if (attacker.RollMelee())
            {
                board.AddToLog(string.Format("{0} connects", attacker.Name));
                board.DownAgent(defender.CurrentX, defender.CurrentY);
            }
 
            if (defender.RollMelee())
            {
                board.AddToLog(string.Format("{0} counters", defender.Name));
                board.DownAgent(attacker.CurrentX, attacker.CurrentY);
            }
       }

        public void MakeRangedAttack(IAgent attacker, IAgent defender)
        {
            board.ClearLog();
            board.AddToLog(string.Format("{0} attacks {1}", attacker.Name, defender.Name));

            if (attacker.RollAttack())
            {
                RangedAttackHits(defender);
            }
        }

        private void RangedAttackHits(IAgent defender)
        {
            board.AddToLog("Attack hits");

            if (defender.RollArmourBreak())
            {
                BreakArmour(defender);
            }
        }

        private void BreakArmour(IAgent defender)
        {
            board.AddToLog("Armour breaks");

            board.DownAgent(defender.CurrentX, defender.CurrentY);
        }
    }
}
