﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Utility;

namespace Assets.Scripts.Logic
{
    public class GraphicsController
    {
        private readonly IGraphicsBehaviour behaviour;
        private readonly int gridSize;

        public GraphicsController(IGraphicsBehaviour behaviour, int gridSize)
        {
            this.behaviour = behaviour;
            this.gridSize = gridSize;
        }

        public void SpawnAllPurpleBase(bool[,] purpleBase)
        {
            for (int y = 0; y < gridSize; y++)
            {
                for (int x = 0; x < gridSize; x++)
                {
                    if (purpleBase[x, y])
                    {
                        behaviour.RenderPurpleBase(x, y);
                    }
                }
            }
        }

        public void SpawnAllOrangeBase(bool[,] orangeBase)
        {
            for (int y = 0; y < gridSize; y++)
            {
                for (int x = 0; x < gridSize; x++)
                {
                    if (orangeBase[x, y])
                    {
                        behaviour.RenderOrangeBase(x, y);
                    }
                }
            }
        }

        public void SpawnWalls(bool[,] walls)
        {
            var renderedWalls = new bool[gridSize, gridSize];

            var wallAdditionalLength = 4;

            while (renderedWalls != walls && wallAdditionalLength >= 0)
            {
                for (int y = 0; y < gridSize; y++)
                {
                    for (int x = 0; x < gridSize; x++)
                    {
                        RenderWall(walls, renderedWalls, wallAdditionalLength, y, x);
                    }
                }

                wallAdditionalLength--;
            }
        }

        private void RenderWall(bool[,] walls, bool[,] renderedWalls, int wallAdditionalLength, int y, int x)
        {
            if (walls[x, y] && !renderedWalls[x, y])
            {
                if (x + wallAdditionalLength < gridSize && ConsecutiveHorizontalWallsUpToCurrentWallLength(walls, renderedWalls, wallAdditionalLength, x, y))
                {
                    RenderHorizontalWall(renderedWalls, wallAdditionalLength, y, x);
                }
                else if (y + wallAdditionalLength < gridSize && ConsecutiveVerticalWallsUpToCurrentWallLength(walls, renderedWalls, wallAdditionalLength, x, y))
                {
                    RenderVerticalWall(renderedWalls, wallAdditionalLength, y, x);
                }
            }
        }

        private bool ConsecutiveHorizontalWallsUpToCurrentWallLength(bool[,] walls, bool[,] renderedWalls, int wallAdditionalLength, int startX, int y)
        {
            for (int i = 0; i <= wallAdditionalLength; i++)
            {
                var currentX = startX + i;
                if (!walls[currentX, y] || renderedWalls[currentX, y])
                {
                    return false;
                }

            }
            return true;
        }

        private void RenderHorizontalWall(bool[,] renderedWalls, int wallAdditionalLength, int y, int x)
        {
            RenderWallForLength(wallAdditionalLength, x, y, WallOrientation.Horizontal);
            FillInHorizontalWalls(renderedWalls, wallAdditionalLength, x, y);
        }

        private void RenderWallForLength(int wallAdditionalLength, int x, int y, WallOrientation orientation)
        {
            switch (wallAdditionalLength)
            {
                case 4:
                    behaviour.RenderFivePieceWall(x, y, orientation);
                    break;
                case 3:
                    behaviour.RenderFourPieceWall(x, y, orientation);
                    break;
                case 2:
                    behaviour.RenderThreePieceWall(x, y, orientation);
                    break;
                case 1:
                    behaviour.RenderTwoPieceWall(x, y, orientation);
                    break;
                default:
                    behaviour.RenderWall(x, y);
                    break;
            }
        }
        private void FillInHorizontalWalls(bool[,] renderedWalls, int wallAdditionalLength, int startX, int y)
        {
            for (int i = 0; i <= wallAdditionalLength; i++)
            {
                renderedWalls[startX + i, y] = true;
            }
        }

        private bool ConsecutiveVerticalWallsUpToCurrentWallLength(bool[,] walls, bool[,] renderedWalls, int wallAdditionalLength, int x, int startY)
        {
            for (int i = 0; i <= wallAdditionalLength; i++)
            {
                var currentY = startY + i;
                if (!walls[x, currentY] || renderedWalls[x, currentY])
                {
                    return false;
                }
            }

            return true;
        }

        private void RenderVerticalWall(bool[,] renderedWalls, int wallAdditionalLength, int y, int x)
        {
            RenderWallForLength(wallAdditionalLength, x, y, WallOrientation.Vertical);
            FillInVerticalWalls(renderedWalls, wallAdditionalLength, x, y);
        }

        private void FillInVerticalWalls(bool[,] renderedWalls, int wallAdditionalLength, int x, int startY)
        {
            for (int i = 0; i <= wallAdditionalLength; i++)
            {
                renderedWalls[x, startY + i] = true;
            }
        }
    }
}
