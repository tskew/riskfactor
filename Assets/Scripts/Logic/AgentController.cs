﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Utility;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Logic
{
    public class AgentController
    {
        private readonly IAgentBehaviour agentBehaviour;

        public AgentController(IAgentBehaviour agentBehaviour)
        {
            this.agentBehaviour = agentBehaviour;
        }

        public int CurrentX { get; set; }

        public int CurrentY { get; set; }

        public bool HoldingFlag { get; set; }

        public int RemainingMovement { get; set; }

        public bool HasAttacked { get; set; }

        public void RefreshMovement()
        {
            RemainingMovement = agentBehaviour.Speed * 2;
            HasAttacked = false;
        }

        public bool[,] PossibleMoves(IAgent[,] agents, bool[,] terrain)
        {
            var moves = new bool[BoardConstants.GRID_SIZE, BoardConstants.GRID_SIZE];

            AddMovesUpToBlock(moves, i => CurrentX, i => CurrentY + i, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX + i, i => CurrentY + i, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX + i, i => CurrentY, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX + i, i => CurrentY - i, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX, i => CurrentY - i, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX - i, i => CurrentY - i, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX - i, i => CurrentY, agents, terrain);

            AddMovesUpToBlock(moves, i => CurrentX - i, i => CurrentY + i, agents, terrain);

            return moves;
        }

        private void AddMovesUpToBlock(bool[,] moves, Func<int, int> xIterator, Func<int, int> yIterator, IAgent[,] agents, bool[,] terrain)
        {
            int loop = 0;

            while (true)
            {
                loop++;

                if (loop > RemainingMovement)
                {
                    break;
                }

                var nextX = xIterator(loop);
                var nextY = yIterator(loop);

                if (nextX >= 0 && nextY >= 0 && nextX < BoardConstants.GRID_SIZE && nextY < BoardConstants.GRID_SIZE)
                {
                    if (agents[nextX, nextY] == null && !terrain[nextX, nextY])
                    {
                        moves[nextX, nextY] = true;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }

            }
        }

        public void MoveTo(int x, int y)
        {
            int distance = 0;
            if (CurrentX != x)
            {
                distance = Math.Abs(CurrentX - x);
            }
            else
            {
                distance = Math.Abs(CurrentY - y);
            }

            RemainingMovement -= distance;
            SetPosition(x, y);
        }

        public void SetPosition(int x, int y)
        {
            CurrentX = x;
            CurrentY = y;
            agentBehaviour.SetTransformPosition(x, y);
        }

        public void PickupFlag()
        {
            HoldingFlag = true;
            RemainingMovement = 0;
        }

        public bool[,] PossibleRangedAttacks(IAgent[,] agents, bool[,] terrain)
        {
            return GetPossibleAttacksUpToRange(agents, terrain, agentBehaviour.Range);
        }

        private bool[,] GetPossibleAttacksUpToRange(IAgent[,] agents, bool[,] terrain, int range)
        {
            var attacks = new bool[BoardConstants.GRID_SIZE, BoardConstants.GRID_SIZE];

            if (!HasAttacked)
            {
                var currentLocation = new Point(CurrentX, CurrentY);

                int minX = CurrentX - range, maxX = CurrentX + range, minY = CurrentY - range, maxY = CurrentY + range;
                for (int i = minX; i <= maxX; i++)
                {
                    AddAttacksUpToBlock(attacks, GetLineToTarget(currentLocation, new Point(i, maxY)), agents, terrain);
                    AddAttacksUpToBlock(attacks, GetLineToTarget(currentLocation, new Point(i, minY)), agents, terrain);
                }

                for (int i = minY; i <= maxY; i++)
                {
                    AddAttacksUpToBlock(attacks, GetLineToTarget(currentLocation, new Point(minX, i)), agents, terrain);
                    AddAttacksUpToBlock(attacks, GetLineToTarget(currentLocation, new Point(maxX, i)), agents, terrain);
                }
            }

            return attacks;
        }

        private void AddAttacksUpToBlock(bool[,] attacks, List<Point> lineToTarget, IAgent[,] agents, bool[,] terrain)
        {
            int loop = 0;

            while (true)
            {
                loop++;

                if (loop >= lineToTarget.Count)
                {
                    break;
                }

                var nextX = lineToTarget[loop].x;
                var nextY = lineToTarget[loop].y;

                if (nextX >= 0 && nextY >= 0 && nextX < BoardConstants.GRID_SIZE && nextY < BoardConstants.GRID_SIZE)
                {
                    if (terrain[nextX, nextY])
                    {
                        break;
                    }
                    else
                    {
                        attacks[nextX, nextY] = true;

                        IAgent agentAtSpace = agents[nextX, nextY];
                        if (agentAtSpace != null)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Bresenham's Line Algorithm
        /// </summary>
        /// <returns>An ordered list of Points leading from the origin to the target</returns>
        private List<Point> GetLineToTarget(Point origin, Point target)
        {
            var points = new List<Point>();

            int x0 = origin.x, y0 = origin.y, x1 = target.x, y1 = target.y;

            bool steep = Math.Abs(y1 - y0) > Math.Abs(x1 - x0);
            if (steep)
            {
                Swap(ref x0, ref y0);
                Swap(ref x1, ref y1);
            }

            bool reversed = false;
            if (x0 > x1)
            {
                Swap(ref x0, ref x1);
                Swap(ref y0, ref y1);
                reversed = true;
            }

            int dx = x1 - x0;
            int dy = Math.Abs(y1 - y0);
            int err = dx / 2;
            int yStep = y0 < y1 ? 1 : -1;
            int y = y0;

            for (int x = x0; x <= x1; x++)
            {
                if (steep)
                {
                    points.Add(new Point(y, x));
                }
                else
                {
                    points.Add(new Point(x, y));
                }

                err -= dy;
                if (err < 0)
                {
                    y += yStep;
                    err += dx;
                }
            }

            if (reversed)
            {
                points.Reverse();
            }

            return points;
        }

        private void Swap<T>(ref T lhs, ref T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        public bool[,] PossibleMeleeAttacks(IAgent[,] agents, bool[,] terrain)
        {
            return GetPossibleAttacksUpToRange(agents, terrain, 1);
        }

        public bool[,] ValidAttacks(bool[,] possibleAttacks, IAgent[,] agents)
        {
            var validAttacks = new bool[BoardConstants.GRID_SIZE, BoardConstants.GRID_SIZE];

            for (int i = 0; i < BoardConstants.GRID_SIZE; i++)
            {
                for (int j = 0; j < BoardConstants.GRID_SIZE; j++)
                {
                    if (possibleAttacks[i, j])
                    {
                        var agent = agents[i, j];

                        if (agent != null && agent.PurpleTeam != agentBehaviour.PurpleTeam)
                        {
                            validAttacks[i, j] = true;
                            possibleAttacks[i, j] = false;
                        }
                    }
                }
            }
            return validAttacks;
        }

        public bool RollMeleeAttack()
        {
            RemainingMovement = 0;
            HasAttacked = true;

            return RollStatCheck(agentBehaviour.Strength, "Str");
        }

        private bool RollStatCheck(int stat, string description)
        {
            var roll = agentBehaviour.Dice.RollD6();
            agentBehaviour.AddToLog(string.Format("Rolled {0}: (<= {1} {2})", roll, description, stat));
            return roll != 6 && (roll == 1 || roll <= stat);
        }

        public bool RollRangedAttack()
        {
            RemainingMovement = 0;
            HasAttacked = true;

            return RollStatCheck(agentBehaviour.Marksmanship, "Marks");
        }

        public bool RollArmourBreak()
        {
            var roll = agentBehaviour.Dice.RollD6();
            agentBehaviour.AddToLog(string.Format("Rolled {0}: (> Arm {1})", roll, agentBehaviour.Armour));
            return roll != 1 && (roll == 6 || roll > agentBehaviour.Armour);
        }
    }
}
