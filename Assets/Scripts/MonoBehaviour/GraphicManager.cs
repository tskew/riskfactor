﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Utility;
using UnityEngine;
using Assets.Scripts.Logic;

public class GraphicManager : MonoBehaviour, IGraphicsBehaviour
{
    public static GraphicManager Instance { get; set; }
    public GraphicsController controller;

    public GameObject wall1x1Prefab;
    public GameObject wall2x1Prefab;
    public GameObject wall3x1Prefab;
    public GameObject wall4x1Prefab;
    public GameObject wall5x1Prefab;

    public GameObject purpleBasePrefab;
    public GameObject orangeBasePrefab;

    void Awake()
    {
        if (controller == null)
        {
            controller = new GraphicsController(this, BoardConstants.GRID_SIZE);
        }
    }

    void Start()
    {
        Instance = this;
    }

    public void SpawnTerrain(TerrainDetailsDto terrain)
    {
        controller.SpawnAllPurpleBase(terrain.PurpleBase);
        controller.SpawnAllOrangeBase(terrain.OrangeBase);
        controller.SpawnWalls(terrain.ImpassableTerrain);
    }

    public void RenderPurpleBase(int x, int y)
    {
        GameObject go = Instantiate(purpleBasePrefab, GetCenterPoint(x, x + 1, y, y + 1), Quaternion.identity) as GameObject;
        go.transform.SetParent(transform);
    }

    private Vector3 GetCenterPoint(int startX, int endX, int startY, int endY)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (BoardConstants.TILE_SIZE * startX) + (((BoardConstants.TILE_SIZE * endX) - (BoardConstants.TILE_SIZE * startX)) / 2);
        origin.z += (BoardConstants.TILE_SIZE * startY) + (((BoardConstants.TILE_SIZE * endY) - (BoardConstants.TILE_SIZE * startY)) / 2);

        return origin;
    }

    public void RenderOrangeBase(int x, int y)
    {
        GameObject go = Instantiate(orangeBasePrefab, GetCenterPoint(x, x + 1, y, y + 1), Quaternion.identity) as GameObject;
        go.transform.SetParent(transform);
    }

    public void RenderWall(int x, int y)
    {
        GameObject go = Instantiate(wall1x1Prefab, GetCenterPoint(x, x + 1, y, y + 1), Quaternion.identity) as GameObject;
        go.transform.SetParent(transform);
    }

    public void RenderTwoPieceWall(int x, int y, WallOrientation orientation)
    {
        Quaternion rotation = GetQuaternion(orientation);
        var endX = orientation == WallOrientation.Horizontal ? x + 2 : x + 1;
        var endY = orientation == WallOrientation.Vertical ? y + 2 : y + 1;
        GameObject go = Instantiate(wall2x1Prefab, GetCenterPoint(x, endX, y, endY), rotation) as GameObject;
        go.transform.SetParent(transform);
    }

    private Quaternion GetQuaternion(WallOrientation orientation)
    {
        switch (orientation)
        {
            case WallOrientation.Horizontal:
                return Quaternion.identity;
            case WallOrientation.Vertical:
                return Quaternion.AngleAxis(90, Vector3.up);
            default:
                return Quaternion.identity;
        }
    }

    public void RenderThreePieceWall(int x, int y, WallOrientation orientation)
    {
        Quaternion rotation = GetQuaternion(orientation);
        var endX = orientation == WallOrientation.Horizontal ? x + 3 : x + 1;
        var endY = orientation == WallOrientation.Vertical ? y + 3 : y + 1;
        GameObject go = Instantiate(wall3x1Prefab, GetCenterPoint(x, endX, y, endY), rotation) as GameObject;
        go.transform.SetParent(transform);
    }

    public void RenderFourPieceWall(int x, int y, WallOrientation orientation)
    {
        Quaternion rotation = GetQuaternion(orientation);
        var endX = orientation == WallOrientation.Horizontal ? x + 4 : x + 1;
        var endY = orientation == WallOrientation.Vertical ? y + 4 : y + 1;
        GameObject go = Instantiate(wall4x1Prefab, GetCenterPoint(x, endX, y, endY), rotation) as GameObject;
        go.transform.SetParent(transform);
    }

    public void RenderFivePieceWall(int x, int y, WallOrientation orientation)
    {
        Quaternion rotation = GetQuaternion(orientation);
        var endX = orientation == WallOrientation.Horizontal ? x + 5 : x + 1;
        var endY = orientation == WallOrientation.Vertical ? y + 5 : y + 1;
        GameObject go = Instantiate(wall5x1Prefab, GetCenterPoint(x, endX, y, endY), rotation) as GameObject;
        go.transform.SetParent(transform);
    }
}
