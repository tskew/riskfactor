﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Utility;
using System.Collections.Generic;
using UnityEngine;

public class BoardHighlights : MonoBehaviour
{
    public static BoardHighlights Instance { get; set; }

    public GameObject highlightPrefab;
    private List<GameObject> highlights;

    public GameObject possibleAttackPrefab;
    private List<GameObject> possibleAttacks;

    public GameObject attackPrefab;
    private List<GameObject> validAttacks;

    public GameObject selectedAgentPrefab;
    private GameObject selectedAgent;

    private void Start()
    {
        Instance = this;
        highlights = new List<GameObject>();
        possibleAttacks = new List<GameObject>();
        validAttacks = new List<GameObject>();
        selectedAgent = Instantiate(selectedAgentPrefab);
    }

    public void HightlightPossibleAttacksFromSelectedAgent(IAgent agent, bool[,] possibleAttacks)
    {
        HighlightSelectedAgent(agent);

        HighlightPossibleAttacks(agent, possibleAttacks);
    }

    public void HighlightSelectedAgent(IAgent agent)
    {
        if (agent != null)
        {
            selectedAgent.SetActive(true);
            selectedAgent.transform.position = new Vector3(agent.CurrentX + 0.5f, 0, agent.CurrentY + 0.5f);
        }
    }

    public void HighlightPossibleAttacks(IAgent agent, bool[,] possibleAttacks)
    {
        if (agent != null)
        {
            for (int i = 0; i < BoardConstants.GRID_SIZE; i++)
            {
                for (int j = 0; j < BoardConstants.GRID_SIZE; j++)
                {
                    if (possibleAttacks[i, j])
                    {
                        GameObject go = GetPossibleAttackObject();
                        go.SetActive(true);
                        go.transform.position = new Vector3(i + 0.5f, 0, j + 0.5f);
                    }
                }
            }
        }
    }

    private GameObject GetPossibleAttackObject()
    {
        GameObject go = possibleAttacks.Find(x => !x.activeSelf);

        if (go == null)
        {
            go = Instantiate(possibleAttackPrefab);
            possibleAttacks.Add(go);
        }

        return go;
    }

    public void HighlightMovesAndAttacksFromSelectedAgent(IAgent agent, bool[,] moves, bool[,] validAttacks)
    {
        HighlightSelectedAgent(agent);

        HighlightAllowedMoves(agent, moves);
        HighlightValidAttacks(agent, validAttacks);
    }

    public void HighlightAllowedMoves(IAgent agent, bool[,] moves)
    {
        if (agent != null)
        {
            for (int i = 0; i < BoardConstants.GRID_SIZE; i++)
            {
                for (int j = 0; j < BoardConstants.GRID_SIZE; j++)
                {
                    if (moves[i, j])
                    {
                        GameObject go = GetHighlightObject();
                        go.SetActive(true);
                        go.transform.position = new Vector3(i + 0.5f, 0, j + 0.5f);
                    }
                }
            }
        }
    }

    private GameObject GetHighlightObject()
    {
        GameObject go = highlights.Find(x => !x.activeSelf);

        if (go == null)
        {
            go = Instantiate(highlightPrefab);
            highlights.Add(go);
        }

        return go;
    }

    public void HideHighlights()
    {
        foreach (GameObject go in highlights)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in possibleAttacks)
        {
            go.SetActive(false);
        }

        foreach (GameObject go in validAttacks)
        {
            go.SetActive(false);
        }

        selectedAgent.SetActive(false);
    }

    public void HighlightValidAttacks(IAgent agent, bool[,] attacks)
    {
        if (agent != null)
        {
            for (int i = 0; i < BoardConstants.GRID_SIZE; i++)
            {
                for (int j = 0; j < BoardConstants.GRID_SIZE; j++)
                {
                    if (attacks[i, j])
                    {
                        GameObject go = GetAttackObject();
                        go.SetActive(true);
                        go.transform.position = new Vector3(i + 0.5f, 0, j + 0.5f);
                    }
                }
            }
        }
    }

    private GameObject GetAttackObject()
    {
        GameObject go = validAttacks.Find(x => !x.activeSelf);

        if (go == null)
        {
            go = Instantiate(attackPrefab);
            validAttacks.Add(go);
        }

        return go;
    }
}
