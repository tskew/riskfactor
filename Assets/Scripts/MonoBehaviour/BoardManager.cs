﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Logic;
using Assets.Scripts.Utility;
using Assets.Scripts.Utility.JsonDto;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BoardManager : MonoBehaviour, IBoardBehaviour
{
    public static BoardManager Instance { get; set; }
    private BoardController controller;

    public int SelectionX { get; private set; }
    public int SelectionY { get; private set; }

    public GameObject blueCamera;
    public GameObject redCamera;
    public Sprite purpleEndTurn;
    public Sprite orangeEndTurn;

    public GameObject endTurnButton;
    public Text log;
    public Text selectedAgent;
    public Text selectedAgentStats;
    public Text flagHolder;

    public List<GameObject> agentPrefabs;
    private List<GameObject> activeAgents;

    public GameObject flagPrefab;
    private GameObject flag;

    public AudioClip clickOn;
    public AudioClip clickOff;

    private void Awake()
    {
        if (controller == null)
        {
            controller = new BoardController(this, new AttackManager(this), BoardConstants.GRID_SIZE);
        }
    }

    private void Start()
    {
        Instance = this;
        SelectionX = -1;
        SelectionY = -1;

        activeAgents = new List<GameObject>();

        SpawnMap();
        GraphicManager.Instance.SpawnTerrain(new TerrainDetailsDto()
        {
            PurpleBase = controller.PurpleBase,
            OrangeBase = controller.OrangeBase,
            ImpassableTerrain = controller.ImpassableTerrain
        });

        ColourEndTurnPurple();
        RefreshAgentMovements();
        RoundPersistence.Instance.Outcome = RoundOutcome.None;
    }

    public Vector3 GetTileCentre(int x, int y)
    {
        Vector3 origin = Vector3.zero;
        origin.x += (BoardConstants.TILE_SIZE * x) + BoardConstants.TILE_OFFSET;
        origin.z += (BoardConstants.TILE_SIZE * y) + BoardConstants.TILE_OFFSET;

        return origin;
    }

    private void SpawnMap()
    {
        var key = RoundPersistence.Instance != null ? RoundPersistence.Instance.MapKey : new SerialisedKey();
        var map = RoundPersistence.Instance != null ? RoundPersistence.Instance.Map.layout : new SerialisedMapRow[BoardConstants.GRID_SIZE];

        for (int row = 0; row < BoardConstants.GRID_SIZE; row++)
        {
            for (int column = 0; column < BoardConstants.GRID_SIZE; column++)
            {
                var currentSpawnCode = map[row].row[column];
                var boardRow = BoardConstants.GRID_SIZE - 1 - row;

                if (currentSpawnCode == key.wall)
                {
                    AddWall(column, boardRow);
                }
                else if (currentSpawnCode == key.purpleRunner)
                {
                    AddPurpleBase(column, boardRow);
                    SpawnUnit(0, column, boardRow, true, "George 'Go-go' Strauss");
                }
                else if (currentSpawnCode == key.purpleBase)
                {
                    AddPurpleBase(column, boardRow);
                }
                else if (currentSpawnCode == key.orangeRunner)
                {
                    AddOrangeBase(column, boardRow);
                    SpawnUnit(1, column, boardRow, false, "Danny 'Taser' Rand");
                }
                else if (currentSpawnCode == key.orangeBase)
                {
                    AddOrangeBase(column, boardRow);
                }
                else if (currentSpawnCode == key.flag)
                {
                    SpawnFlag(column, boardRow);
                }
            }
        }
    }

    private void AddWall(int x, int y)
    {
        controller.ImpassableTerrain[x, y] = true;
    }

    private void AddPurpleBase(int x, int y)
    {
        controller.PurpleBase[x, y] = true;
    }

    private void SpawnUnit(int index, int x, int y, bool blueTeam, string name)
    {
        Quaternion startDirection = blueTeam ? Quaternion.identity : Quaternion.AngleAxis(180, Vector3.up);
        GameObject go = Instantiate(agentPrefabs[index], GetTileCentre(x, y), startDirection) as GameObject;
        go.transform.SetParent(transform);
        controller.Agents[x, y] = go.GetComponent<Agent>();
        controller.Agents[x, y].Name = name;
        controller.Agents[x, y].SpawnAt(x, y);
        activeAgents.Add(go);
    }

    private void AddOrangeBase(int x, int y)
    {
        controller.OrangeBase[x, y] = true;
    }

    public void SpawnFlag(int x, int y)
    {
        if (flag == null)
        {
            GameObject go = Instantiate(flagPrefab, GetTileCentre(x, y), Quaternion.identity) as GameObject;
            go.transform.SetParent(transform);
            flag = go;
        }
        else
        {
            flag.transform.position = GetTileCentre(x, y);
            flag.SetActive(true);
        }

        controller.FlagLocation = new Point(x, y);
        ShowFlagHolder("");
    }

    public void FlagPickedUp()
    {
        flag.SetActive(false);
        controller.FlagLocation = null;
    }

    public void EndTurn()
    {
        controller.EndTurn();
    }

    public void ColourEndTurnOrange()
    {
        endTurnButton.GetComponent<Image>().sprite = orangeEndTurn;
        EnableCamera(redCamera);
        DisableCamera(blueCamera);
    }

    private void EnableCamera(GameObject camera)
    {
        camera.GetComponent<Camera>().enabled = true;
        camera.GetComponentInChildren<MeshRenderer>().enabled = true;
    }

    private void DisableCamera(GameObject camera)
    {
        camera.GetComponent<Camera>().enabled = false;
        camera.GetComponentInChildren<MeshRenderer>().enabled = false;
    }

    public void ColourEndTurnPurple()
    {
        endTurnButton.GetComponent<Image>().sprite = purpleEndTurn;
        EnableCamera(blueCamera);
        DisableCamera(redCamera);
    }

    public void RefreshAgentMovements()
    {
        activeAgents.ForEach(x => x.GetComponent<Agent>().RefreshMovement());
    }

    public void DownAgent(int x, int y)
    {
        activeAgents.Single(a =>
        {
            var agent = a.GetComponent<Agent>();
            return a.activeSelf && agent.Active && agent.CurrentX == x && agent.CurrentY == y;
        }).GetComponent<Animator>().SetTrigger("down");

        controller.AgentDowned(x, y);
    }

    public void Reset()
    {
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }

    public void EndRound(RoundOutcome outcome)
    {
        RoundPersistence.Instance.Outcome = outcome;
        SceneManager.LoadScene("EndRound", LoadSceneMode.Single);
    }

    public void MoveActionSelected()
    {
        controller.SelectedAction = ActionType.Move;
    }

    public void ShootActionSelected()
    {
        controller.SelectedAction = ActionType.Shoot;
    }

    public void MeleeActionSelected()
    {
        controller.SelectedAction = ActionType.Melee;
    }

    private void Update()
    {
        UpdateSelection();

        if (Input.GetMouseButtonDown(0))
        {
            controller.BoardClicked(SelectionX, SelectionY);
            controller.CheckForRoundEnd();
        }
        else if (Input.GetMouseButtonDown(1))
        {
            if (controller.SelectedAgent != null)
            {
                AudioSource.PlayClipAtPoint(clickOff, transform.position);
            }

            controller.SelectedAgent = null;
            ClearSelectedAgent();
        }

        UpdateHighlights();
    }

    private void UpdateSelection()
    {
        if (Camera.main)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 25.0f, LayerMask.GetMask("BoardPlane")))
            {
                SelectionX = (int)hit.point.x;
                SelectionY = (int)hit.point.z;
            }
            else
            {
                SelectionX = -1;
                SelectionY = -1;
            }
        }
    }

    public void UpdateHighlights()
    {
        var highlighter = BoardHighlights.Instance;

        highlighter.HideHighlights();

        if (controller.SelectedAgent != null)
        {
            highlighter.HighlightSelectedAgent(controller.SelectedAgent);

            if (controller.SelectedAction == ActionType.Move)
            {
                highlighter.HighlightAllowedMoves(controller.SelectedAgent, controller.AgentActionOptions.PossibleMoves);
            }
            else if (controller.SelectedAction == ActionType.Shoot)
            {
                highlighter.HighlightPossibleAttacks(controller.SelectedAgent, controller.AgentActionOptions.PossibleRanged);
                highlighter.HighlightValidAttacks(controller.SelectedAgent, controller.AgentActionOptions.ValidRanged);
            }
            else if (controller.SelectedAction == ActionType.Melee)
            {
                highlighter.HighlightPossibleAttacks(controller.SelectedAgent, controller.AgentActionOptions.PossibleMelee);
                highlighter.HighlightValidAttacks(controller.SelectedAgent, controller.AgentActionOptions.ValidMelee);
            }
        }
    }

    public void ClearLog()
    {
        log.text = "";
    }

    public void AddToLog(string text)
    {
        log.text += text + "; ";
    }

    public void PlaySelectionSound()
    {
        AudioSource.PlayClipAtPoint(clickOn, transform.position);
    }

    public void ShowSelectedAgent(AgentStats agent)
    {
        selectedAgent.text = string.Format("Selected Agent: {0}", agent.Name);
        selectedAgentStats.text = string.Format("Class - {0}; Speed - {1}; Range - {2}; Marks - {3}; Strength - {4}; Armour - {5}", agent.Class, agent.Speed, agent.Range, agent.Marksmanship, agent.Strength, agent.Armour);
    }

    public void ClearSelectedAgent()
    {
        selectedAgent.text = "Selected Agent: ";
        selectedAgentStats.text = "";
    }

    public void ShowFlagHolder(string name)
    {
        flagHolder.text = string.Format("Flag Holder: {0}", name);
    }
}
