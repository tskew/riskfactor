﻿using Assets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class DiceRoller : MonoBehaviour, IDiceRoller
{
    public static DiceRoller Instance { get; set; }

    private System.Random rand;

    void Start()
    {
        Instance = this;
        rand = new System.Random();
    }

    public int LastRoll { get; private set; }

    public int RollD6()
    {
        LastRoll = rand.Next(1, 7);
        return LastRoll;
    }
}
