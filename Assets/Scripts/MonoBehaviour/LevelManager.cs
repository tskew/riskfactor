﻿using Assets.Scripts.Utility.JsonDto;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    private SerialisedMaps maps;

    public GameObject levelsPanel;
    public GameObject buttonPrefab;

    public void Start()
    {
        var path = Path.Combine(Path.Combine(Application.streamingAssetsPath, "Maps"), "maps.json");
        string mapJson = File.ReadAllText(path);
        maps = JsonUtility.FromJson<SerialisedMaps>(mapJson);

        RoundPersistence.Instance.MapKey = maps.key;
        GenerateButtonsForLevels();
    }

    private void GenerateButtonsForLevels()
    {
        foreach (SerialisedMap map in maps.maps)
        {
            GameObject go = Instantiate(buttonPrefab, levelsPanel.transform);
            Button button = go.GetComponent<Button>();
            AddListener(button, map.name);

            Text text = go.GetComponentInChildren<Text>();
            text.text = map.name;
        }
    }

    private void AddListener(Button b, string identifier)
    {
        b.onClick.AddListener(() => LevelButtonClicked(identifier));
    }

    public void LevelButtonClicked(string identifier)
    {
        RoundPersistence.Instance.Map = maps.maps.First(x => x.name == identifier);
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }
}
