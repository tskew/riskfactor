﻿using Assets.Scripts.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoundManger : MonoBehaviour
{
    public Sprite purpleWin;
    public Sprite orangeWin;

    public GameObject backgroundPanel;

    public void Start()
    {
        var outcome = RoundPersistence.Instance.Outcome;

        switch (outcome)
        {
            case RoundOutcome.OrangeByCapture:
            case RoundOutcome.OrangeByElimination:
                backgroundPanel.GetComponent<Image>().sprite = orangeWin;
                break;
            case RoundOutcome.PurpleByCapture:
            case RoundOutcome.PurpleByElimination:
                backgroundPanel.GetComponent<Image>().sprite = purpleWin;
                break;
            default:
                break;
        }
    }

    public void Reset()
    {
        SceneManager.LoadScene("Main", LoadSceneMode.Single);
    }
}
