﻿using Assets.Scripts.Utility.JsonDto;
using System.IO;
using System.Linq;
using UnityEngine;

public class TestSetupScript : MonoBehaviour
{
    public void Awake()
    {
        if (RoundPersistence.Instance.Map == null)
        {
            var path = Path.Combine(Path.Combine(Application.streamingAssetsPath, "Maps"), "maps.json");
            string mapJson = File.ReadAllText(path);
            var maps = JsonUtility.FromJson<SerialisedMaps>(mapJson);

            RoundPersistence.Instance.MapKey = maps.key;
            RoundPersistence.Instance.Map = maps.maps.First();
        }
    }
}
