﻿using Assets.Scripts.Utility.JsonDto;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.MonoBehaviour.Classes
{
    public class Techie : Agent
    {
        protected override void SetStatsFromFile()
        {
            var path = Path.Combine(Path.Combine(Application.streamingAssetsPath, "Classes"), "techie.json");
            string agentJson = File.ReadAllText(path);
            SerialisedAgent agent = JsonUtility.FromJson<SerialisedAgent>(agentJson);

            if (agent != null)
            {
                speed = agent.speed;
                range = agent.range;
                marksmanship = agent.marksmanship;
                strength = agent.strength;
                armour = agent.armour;
            }
        }

        public int armour;
        public override int Armour
        {
            get
            {
                return armour;
            }
        }

        public int marksmanship;
        public override int Marksmanship
        {
            get
            {
                return marksmanship;
            }
        }

        public int range;
        public override int Range
        {
            get
            {
                return range;
            }
        }

        public int speed;
        public override int Speed
        {
            get
            {
                return speed;
            }
        }

        public int strength;
        public override int Strength
        {
            get
            {
                return strength;
            }
        }
    }
}
