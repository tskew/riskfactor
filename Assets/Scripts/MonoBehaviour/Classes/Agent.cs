﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Logic;
using Assets.Scripts.Utility;
using UnityEngine;

public abstract class Agent : MonoBehaviour, IAgent, IAgentBehaviour
{
    private AgentController controller;

    private void Awake()
    {
        if (controller == null)
        {
            controller = new AgentController(this);
        }
    }

    public void Start()
    {
        GunSound = GetComponent<AudioSource>();
        Active = true;

        SetStatsFromFile();
    }

    protected abstract void SetStatsFromFile();

    public IDiceRoller Dice
    {
        get
        {
            return DiceRoller.Instance;
        }
    }

    public bool Active { get; private set; }

    public int CurrentX
    {
        get
        {
            return controller.CurrentX;
        }
    }

    public int CurrentY
    {
        get
        {
            return controller.CurrentY;
        }
    }

    public int RemainingMovement
    {
        get
        {
            return controller.RemainingMovement;
        }
    }

    protected AudioSource GunSound { get; set; }

    public string Name { get; set; }

    public abstract int Speed { get; }

    public abstract int Range { get; }

    public abstract int Marksmanship { get; }

    public abstract int Strength { get; }

    public abstract int Armour { get; }

    public AgentStats GetAgentStats()
    {
        return new AgentStats()
        {
            Name = Name,
            Class = GetType().Name,
            Speed = Speed,
            Range = Range,
            Marksmanship = Marksmanship,
            Strength = Strength,
            Armour = Armour
        };
    }

    public bool purpleTeam;
    public bool PurpleTeam
    {
        get
        {
            return purpleTeam;
        }
    }

    public bool HoldingFlag
    {
        get
        {
            return controller.HoldingFlag;
        }
    }

    public void PickupFlag()
    {
        controller.PickupFlag();
    }

    public void DropFlag()
    {
        controller.HoldingFlag = false;
    }

    public void SetTransformPosition(int x, int y)
    {
        transform.position = BoardManager.Instance.GetTileCentre(x, y);
    }

    public void SpawnAt(int x, int y)
    {
        controller.SetPosition(x, y);
    }

    public void RefreshMovement()
    {
        controller.RefreshMovement();
    }

    public AgentOptions GetActionOptions(IAgent[,] agents, bool[,] terrain)
    {
        var options = new AgentOptions(BoardConstants.GRID_SIZE)
        {
            PossibleMoves = controller.PossibleMoves(agents, terrain),
            PossibleRanged = controller.PossibleRangedAttacks(agents, terrain),
            PossibleMelee = controller.PossibleMeleeAttacks(agents, terrain)
        };

        options.ValidRanged = controller.ValidAttacks(options.PossibleRanged, agents);
        options.ValidMelee = controller.ValidAttacks(options.PossibleMelee, agents);

        return options;
    }

    public void MoveTo(int x, int y)
    {
        controller.MoveTo(x, y);
    }

    public bool RollAttack()
    {
        GunSound.Play();
        return controller.RollRangedAttack();
    }

    public bool RollMelee()
    {
        return controller.RollMeleeAttack();
    }

    public bool RollArmourBreak()
    {
        return controller.RollArmourBreak();
    }

    public void Down()
    {
        Active = false;
    }

    public void AddToLog(string text)
    {
        BoardManager.Instance.AddToLog(text);
    }
}
