﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PlayPressed()
    {
        SceneManager.LoadScene("Levels", LoadSceneMode.Single);
    }

    public void ExitPressed()
    {
        Application.Quit();
    }
}
