﻿using Assets.Scripts.Utility;
using Assets.Scripts.Utility.JsonDto;
using UnityEngine;

public class RoundPersistence : MonoBehaviour
{
    public static RoundPersistence Instance { get; set; }

    public SerialisedMap Map { get; set; }

    public SerialisedKey MapKey { get; set; }

    public RoundOutcome Outcome { get; set; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
    }
}
