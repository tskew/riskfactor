﻿using Assets.Scripts.Utility;

namespace Assets.Scripts.Interfaces
{
    public interface IAgent
    {
        string Name { get; set; }

        AgentStats GetAgentStats();

        bool PurpleTeam { get; }

        bool Active { get; }

        bool HoldingFlag { get; }

        void PickupFlag();

        void DropFlag();

        int CurrentX { get; }

        int CurrentY { get; }

        int RemainingMovement { get; }

        void SpawnAt(int x, int y);

        AgentOptions GetActionOptions(IAgent[,] agents, bool[,] terrain);

        void MoveTo(int x, int y);

        bool RollAttack();

        bool RollMelee();

        bool RollArmourBreak();

        void Down();
    }
}
