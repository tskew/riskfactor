﻿namespace Assets.Scripts.Interfaces
{
    public interface IAgentBehaviour
    {
        IDiceRoller Dice { get; }

        bool PurpleTeam { get; }

        int Speed { get; }

        int Range { get; }

        int Marksmanship { get; }

        int Strength { get; }

        int Armour { get; }

        void SetTransformPosition(int x, int y);

        void AddToLog(string text);
    }
}
