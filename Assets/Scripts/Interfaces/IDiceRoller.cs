﻿namespace Assets.Scripts.Interfaces
{
    public interface IDiceRoller
    {
        int LastRoll { get; }

        int RollD6();
    }
}
