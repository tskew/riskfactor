﻿using Assets.Scripts.Utility;

namespace Assets.Scripts.Interfaces
{
    public interface IGraphicsBehaviour
    {
        void RenderPurpleBase(int x, int y);

        void RenderOrangeBase(int x, int y);

        void RenderWall(int x, int y);

        void RenderTwoPieceWall(int x, int y, WallOrientation orientation);

        void RenderThreePieceWall(int x, int y, WallOrientation orientation);

        void RenderFourPieceWall(int x, int y, WallOrientation orientation);

        void RenderFivePieceWall(int x, int y, WallOrientation orientation);
    }
}
