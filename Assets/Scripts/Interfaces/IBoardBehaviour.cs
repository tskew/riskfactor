﻿using Assets.Scripts.Utility;

namespace Assets.Scripts.Interfaces
{
    public interface IBoardBehaviour
    {
        void SpawnFlag(int x, int y);

        void FlagPickedUp();

        void ColourEndTurnPurple();

        void ColourEndTurnOrange();

        void RefreshAgentMovements();

        void DownAgent(int x, int y);

        void EndRound(RoundOutcome outcome);

        void ClearLog();

        void AddToLog(string text);

        void PlaySelectionSound();

        void ShowSelectedAgent(AgentStats agent);

        void ClearSelectedAgent();

        void ShowFlagHolder(string name);
    }
}
