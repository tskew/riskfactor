﻿namespace Assets.Scripts.Interfaces
{
    public interface IAttackManager
    {
        void MakeMeleeAttacks(IAgent attacker, IAgent defender);

        void MakeRangedAttack(IAgent attacker, IAgent defender);
    }
}
