﻿namespace Assets.Scripts.Utility
{
    public enum RoundOutcome
    {
        None,

        PurpleByElimination,
        PurpleByCapture,
        OrangeByElimination,
        OrangeByCapture,
        Draw
    }
}
