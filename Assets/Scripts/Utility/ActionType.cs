﻿namespace Assets.Scripts.Utility
{
    public enum ActionType
    {
        Move,
        Shoot,
        Melee
    }
}
