﻿namespace Assets.Scripts.Utility
{
    public static class BoardConstants
    {
        public const int GRID_SIZE = 17;
        public const float TILE_SIZE = 1.0f;
        public const float TILE_OFFSET = 0.5f;
    }
}
