﻿namespace Assets.Scripts.Utility
{
    public class TerrainDetailsDto
    {
        public bool[,] ImpassableTerrain { get; set; }

        public bool[,] PurpleBase { get; set; }

        public bool[,] OrangeBase { get; set; }

        public Point Flag { get; set; }
    }
}
