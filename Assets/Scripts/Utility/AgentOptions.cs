﻿namespace Assets.Scripts.Utility
{
    public class AgentOptions
    {
        private int gridSize;

        public AgentOptions(int gridSize)
        {
            this.gridSize = gridSize;

            PossibleMoves = new bool[gridSize, gridSize];
            PossibleRanged = new bool[gridSize, gridSize];
            ValidRanged = new bool[gridSize, gridSize];
            PossibleMelee = new bool[gridSize, gridSize];
            ValidMelee = new bool[gridSize, gridSize];
        }

        public bool[,] PossibleMoves { get; set; }

        public bool[,] PossibleRanged { get; set; }

        public bool[,] ValidRanged { get; set; }

        public bool[,] PossibleMelee { get; set; }

        public bool[,] ValidMelee { get; set; }
    }
}
