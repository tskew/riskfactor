﻿namespace Assets.Scripts.Utility
{
    public class AgentStats
    {
        public string Name { get; set; }

        public string Class { get; set; }

        public int Speed { get; set; }

        public int Range { get; set; }

        public int Marksmanship { get; set; }

        public int Strength { get; set; }

        public int Armour { get; set; }
    }
}
