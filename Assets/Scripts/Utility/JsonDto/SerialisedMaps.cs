﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Utility.JsonDto
{
    [Serializable]
    public class SerialisedMaps
    {
        public SerialisedKey key;

        public List<SerialisedMap> maps;
    }
}
