﻿using System;

namespace Assets.Scripts.Utility.JsonDto
{
    [Serializable]
    public class SerialisedKey
    {
        public string wall;

        public string purpleBase;

        public string orangeBase;

        public string flag;

        public string purpleRunner;

        public string orangeRunner;
    }
}
