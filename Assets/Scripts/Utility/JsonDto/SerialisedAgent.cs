﻿using System;

namespace Assets.Scripts.Utility.JsonDto
{
    [Serializable]
    public class SerialisedAgent
    {
        public int speed;
        public int range;
        public int marksmanship;
        public int strength;
        public int armour;
    }
}
