﻿using System;

namespace Assets.Scripts.Utility.JsonDto
{
    [Serializable]
    public class SerialisedMapRow
    {
        public string[] row;
    }
}
