﻿using System;

namespace Assets.Scripts.Utility.JsonDto
{
    [Serializable]
    public class SerialisedMap
    {
        public string name;

        public MapType type;

        public SerialisedMapRow[] layout;
    }
}
