﻿using System;

namespace Assets.Scripts.Utility
{
    [Serializable]
    public enum MapType
    {
        None,

        Forest,
        Desert,
        Factory
    }
}
