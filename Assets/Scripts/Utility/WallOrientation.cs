﻿namespace Assets.Scripts.Utility
{
    public enum WallOrientation
    {
        Horizontal,
        Vertical
    }
}
