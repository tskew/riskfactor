﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Logic;
using Assets.Scripts.Utility;
using NSubstitute;
using NUnit.Framework;
using System;

namespace Assets.Editor
{
    [TestFixture]
    public class BoardControllerTests
    {
        [Test]
        public void BoardClicked_SelectionXOrYIsBelowZero_NoActionTaken()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            BoardController sut = GetSut(board, 1);

            // Act
            sut.BoardClicked(-1, -1);

            // Assert
            Assert.Null(sut.SelectedAgent);
        }

        private BoardController GetSut(IBoardBehaviour board, int gridSize)
        {
            return new BoardController(board, Substitute.For<IAttackManager>(), gridSize);
        }

        [Test]
        public void BoardClicked_SelectedAgentIsNullButAgentDoesNotExistAtSelectionIndex_DoesNotSetAgent()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            BoardController sut = GetSut(board, 1);

            // Act
            sut.BoardClicked(0, 0);

            // Assert
            Assert.Null(sut.SelectedAgent);
        }

        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(0, 1)]
        public void BoardClicked_SelectedAgentIsNullAndAgentExistsAtSelectionIndex_SetsActiveAgentAndUpdatesHighlights(int selectionX, int selectionY)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(true);

            BoardController sut = GetSut(board, 2);
            sut.PurpleTeamTurn = true;
            sut.Agents[selectionX, selectionY] = agent;

            // Act
            sut.BoardClicked(selectionX, selectionY);

            // Assert
            Assert.AreEqual(agent, sut.SelectedAgent);
            agent.Received().GetActionOptions(Arg.Any<IAgent[,]>(), Arg.Any<bool[,]>());
        }

        [Test]
        public void BoardClicked_SelectedAgentIsNullAndAgentExistsAtSelectionIndexButTeamDoesNotMatchCurrentTurn_SelectsAgentAndResetsPossibleMovesAndAttacks()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(false);

            BoardController sut = GetSut(board, 1);
            sut.AgentActionOptions = new AgentOptions(1);
            sut.AgentActionOptions.PossibleMoves[0, 0] = true;
            sut.Agents[0, 0] = agent;
            sut.PurpleTeamTurn = true;

            // Act
            sut.BoardClicked(0, 0);

            // Assert
            Assert.AreEqual(agent, sut.SelectedAgent);
            agent.DidNotReceive().GetActionOptions(Arg.Any<IAgent[,]>(), Arg.Any<bool[,]>());
            Assert.False(sut.AgentActionOptions.PossibleMoves[0, 0], "Options not reset");
        }

        [Test]
        public void BoardClicked_AgentIsSelectedButSelectionIndexIsNotAValidMoveOrAttack_NoAction()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(true);

            BoardController sut = GetSut(board, 1);
            sut.PurpleTeamTurn = true;
            sut.SelectedAgent = agent;

            sut.AgentActionOptions.PossibleMoves[0, 0] = false;
            sut.AgentActionOptions.ValidRanged[0, 0] = false;
            sut.AgentActionOptions.ValidMelee[0, 0] = false;

            // Act
            sut.BoardClicked(0, 0);

            // Assert
            Assert.AreEqual(agent, sut.SelectedAgent);
        }

        [Test]
        public void BoardClicked_AgentIsSelectedAndSelectedActionIsMoveAndSelectionIndexIsAValidMove_MovesAgentAndUpdatesHighlights()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(true);
            agent.RemainingMovement.Returns(1);

            BoardController sut = GetSut(board, 2);
            sut.SelectedAction = ActionType.Move;
            sut.PurpleTeamTurn = true;
            sut.SelectedAgent = agent;
            sut.Agents[0, 0] = agent;

            sut.AgentActionOptions.PossibleMoves[1, 1] = true;

            // Act
            sut.BoardClicked(1, 1);

            // Assert
            agent.Received().MoveTo(1, 1);
            agent.Received().GetActionOptions(Arg.Any<IAgent[,]>(), Arg.Any<bool[,]>());
            Assert.Null(sut.Agents[0, 0]);
            Assert.AreEqual(agent, sut.Agents[1, 1]);
        }

        [TestCase(ActionType.Shoot)]
        [TestCase(ActionType.Melee)]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidMoveButSelectedActionIsNotMove_DoesNotMove(ActionType selectedAction)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(true);
            agent.RemainingMovement.Returns(1);

            BoardController sut = GetSut(board, 2);
            sut.SelectedAction = selectedAction;
            sut.PurpleTeamTurn = true;
            sut.SelectedAgent = agent;
            sut.Agents[0, 0] = agent;

            sut.AgentActionOptions.PossibleMoves[1, 1] = true;

            // Act
            sut.BoardClicked(1, 1);

            // Assert
            agent.DidNotReceive().MoveTo(Arg.Any<int>(), Arg.Any<int>());
            Assert.AreEqual(agent, sut.Agents[0, 0]);
            Assert.Null(sut.Agents[1, 1]);
        }

        [TestCase(1, 0)]
        [TestCase(0, 1)]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidMoveWhichIsTheFlagLocation_AgentPicksUpTheFlagAndFlagIsRemoved(int x, int y)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(true);
            agent.RemainingMovement.Returns(1);
            agent.HoldingFlag.Returns(false);

            BoardController sut = GetSut(board, 2);
            sut.SelectedAction = ActionType.Move;
            sut.PurpleTeamTurn = true;
            sut.SelectedAgent = agent;
            sut.Agents[0, 0] = agent;
            sut.FlagLocation = new Point(x, y);

            sut.AgentActionOptions.PossibleMoves[x, y] = true;

            // Act
            sut.BoardClicked(x, y);

            // Assert
            agent.Received().PickupFlag();
            board.Received().FlagPickedUp();
        }

        [TestCase(1, 0, 2, 0)]
        [TestCase(0, 1, 0, 2)]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidMoveWhichIsNotTheFlagLocation_DoesNotPickUpTheFlag(int moveX, int moveY, int flagX, int flagY)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.PurpleTeam.Returns(true);
            agent.RemainingMovement.Returns(1);
            agent.HoldingFlag.Returns(false);

            BoardController sut = GetSut(board, 3);
            sut.SelectedAction = ActionType.Move;
            sut.PurpleTeamTurn = true;
            sut.SelectedAgent = agent;
            sut.Agents[0, 0] = agent;
            sut.FlagLocation = new Point(flagX, flagY);

            sut.AgentActionOptions.PossibleMoves[moveX, moveY] = true;

            // Act
            sut.BoardClicked(moveX, moveY);

            // Assert
            agent.DidNotReceive().PickupFlag();
            board.DidNotReceive().FlagPickedUp();
        }

        [Test]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidRangedAttackAndActionIsShoot_CallsAttackManager()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var purpleAgent = Substitute.For<IAgent>();
            var orangeAgent = Substitute.For<IAgent>();

            var attackManager = Substitute.For<IAttackManager>();

            BoardController sut = GetSut(board, attackManager, 2);
            sut.SelectedAction = ActionType.Shoot;
            sut.SelectedAgent = purpleAgent;
            sut.Agents[0, 0] = purpleAgent;
            sut.Agents[1, 1] = orangeAgent;

            sut.AgentActionOptions.ValidRanged[1, 1] = true;

            // Act
            sut.BoardClicked(1, 1);

            // Assert
            attackManager.Received().MakeRangedAttack(purpleAgent, orangeAgent);
        }

        private BoardController GetSut(IBoardBehaviour board, IAttackManager manager, int gridSize)
        {
            return new BoardController(board, manager, gridSize);
        }

        [TestCase(ActionType.Move)]
        [TestCase(ActionType.Melee)]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidRangedAttackButActionIsNotShoot_AttackManagerNotCalled(ActionType selectedAction)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var purpleAgent = Substitute.For<IAgent>();
            var orangeAgent = Substitute.For<IAgent>();

            var attackManager = Substitute.For<IAttackManager>();

            BoardController sut = GetSut(board, attackManager, 2);
            sut.SelectedAction = selectedAction;
            sut.SelectedAgent = purpleAgent;
            sut.Agents[0, 0] = purpleAgent;
            sut.Agents[1, 1] = orangeAgent;

            sut.AgentActionOptions.ValidRanged[1, 1] = true;

            // Act
            sut.BoardClicked(1, 1);

            // Assert
            attackManager.DidNotReceive().MakeRangedAttack(Arg.Any<IAgent>(), Arg.Any<IAgent>());
        }

        [Test]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidMeleeAttackAndActionIsMelee_AttackManagerCalled()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var purpleAgent = Substitute.For<IAgent>();
            var orangeAgent = Substitute.For<IAgent>();

            var attackManager = Substitute.For<IAttackManager>();

            BoardController sut = GetSut(board, attackManager, 2);
            sut.SelectedAction = ActionType.Melee;
            sut.SelectedAgent = purpleAgent;
            sut.Agents[0, 0] = purpleAgent;
            sut.Agents[1, 1] = orangeAgent;

            sut.AgentActionOptions.ValidMelee[1, 1] = true;

            // Act
            sut.BoardClicked(1, 1);

            // Assert
            attackManager.Received().MakeMeleeAttacks(purpleAgent, orangeAgent);
        }

        [TestCase(ActionType.Move)]
        [TestCase(ActionType.Shoot)]
        public void BoardClicked_AgentIsSelectedAndSelectionIndexIsAValidMeleeAttackButActionIsNotMelee_AttackManagerNotCalled(ActionType selectedAction)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var purpleAgent = Substitute.For<IAgent>();
            var orangeAgent = Substitute.For<IAgent>();

            var attackManager = Substitute.For<IAttackManager>();

            BoardController sut = GetSut(board, attackManager, 2);
            sut.SelectedAction = selectedAction;
            sut.SelectedAgent = purpleAgent;
            sut.Agents[0, 0] = purpleAgent;
            sut.Agents[1, 1] = orangeAgent;

            sut.AgentActionOptions.ValidMelee[1, 1] = true;

            // Act
            sut.BoardClicked(1, 1);

            // Assert
            attackManager.DidNotReceive().MakeMeleeAttacks(Arg.Any<IAgent>(), Arg.Any<IAgent>());
        }

        [Test]
        public void AgentDowned_AgentIsNotHoldingFlag_RemovesAgentFromAgentsArray()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.HoldingFlag.Returns(false);

            BoardController sut = GetSut(board, 1);
            sut.Agents[0, 0] = agent;

            // Act
            sut.AgentDowned(0, 0);

            // Assert
            agent.Received().Down();
            Assert.Null(sut.Agents[0, 0]);
            board.DidNotReceive().SpawnFlag(0, 0);
        }

        [Test]
        public void AgentDowned_AgentIsHoldingFlag_AgentDropsFlagAndSpawnsFlagAtLocation()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();
            agent.HoldingFlag.Returns(true);

            BoardController sut = GetSut(board, 1);
            sut.Agents[0, 0] = agent;

            // Act
            sut.AgentDowned(0, 0);

            // Assert
            agent.Received().DropFlag();
            board.Received().SpawnFlag(0, 0);
        }

        [TestCase(0, 0, 1, 1)]
        [TestCase(1, 0, 0, 1)]
        [TestCase(1, 1, 0, 0)]
        [TestCase(0, 1, 1, 0)]
        public void CheckForRoundEnd_AgentsOfBothTeamsStillExist_DoesNotEndRound(int blueX, int blueY, int redX, int redY)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);

            BoardController sut = GetSut(board, 2);
            sut.Agents[blueX, blueY] = blueAgent;
            sut.Agents[redX, redY] = redAgent;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.DidNotReceive().EndRound(Arg.Any<RoundOutcome>());
        }

        [Test]
        public void CheckForRoundEnd_AllRedAgentsAreRemoved_EndsRoundWithBlueWin()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);

            BoardController sut = GetSut(board, 1);
            sut.Agents[0, 0] = blueAgent;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.Received().EndRound(RoundOutcome.PurpleByElimination);
        }

        [Test]
        public void CheckForRoundEnd_AllBlueAgentsAreRemoved_EndsRoundWithRedWin()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);

            BoardController sut = GetSut(board, 1);
            sut.Agents[0, 0] = redAgent;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.Received().EndRound(RoundOutcome.OrangeByElimination);
        }

        [Test]
        public void CheckForRoundEnd_BlueAgentIsHoldingFlagAndIsInBlueBase_EndsRoundWithBlueWin()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);
            blueAgent.HoldingFlag.Returns(true);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);

            BoardController sut = GetSut(board, 2);
            sut.Agents[0, 0] = blueAgent;
            sut.Agents[0, 1] = redAgent;
            sut.PurpleBase[0, 0] = true;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.Received().EndRound(RoundOutcome.PurpleByCapture);
        }

        [Test]
        public void CheckForRoundEnd_BlueAgentIsNotHoldingFlagAndIsInBlueBase_DoesNotEndRound()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);
            blueAgent.HoldingFlag.Returns(false);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);

            BoardController sut = GetSut(board, 2);
            sut.Agents[0, 0] = blueAgent;
            sut.Agents[0, 1] = redAgent;
            sut.PurpleBase[0, 0] = true;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.DidNotReceive().EndRound(Arg.Any<RoundOutcome>());
        }

        [Test]
        public void CheckForRoundEnd_RedAgentIsHoldingFlagAndIsInBlueBase_DoesNotEndRound()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);
            redAgent.HoldingFlag.Returns(true);

            BoardController sut = GetSut(board, 2);
            sut.Agents[0, 0] = redAgent;
            sut.Agents[0, 1] = blueAgent;
            sut.PurpleBase[0, 0] = true;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.DidNotReceive().EndRound(Arg.Any<RoundOutcome>());
        }

        [Test]
        public void CheckForRoundEnd_RedAgentIsHoldingFlagAndIsInRedBase_EndsRoundWithRedWin()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);
            redAgent.HoldingFlag.Returns(true);

            BoardController sut = GetSut(board, 2);
            sut.Agents[0, 0] = blueAgent;
            sut.Agents[1, 1] = redAgent;
            sut.OrangeBase[1, 1] = true;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.Received().EndRound(RoundOutcome.OrangeByCapture);
        }

        [Test]
        public void CheckForRoundEnd_RedAgentIsNotHoldingFlagAndIsInRedBase_DoesNotEndRound()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);
            redAgent.HoldingFlag.Returns(false);

            BoardController sut = GetSut(board, 2);
            sut.Agents[0, 0] = blueAgent;
            sut.Agents[1, 1] = redAgent;
            sut.OrangeBase[1, 1] = true;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.DidNotReceive().EndRound(Arg.Any<RoundOutcome>());
        }

        [Test]
        public void CheckForRoundEnd_BlueAgentIsHoldingFlagAndIsInRedBase_DoesNotEndRound()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var blueAgent = Substitute.For<IAgent>();
            blueAgent.PurpleTeam.Returns(true);
            blueAgent.HoldingFlag.Returns(true);

            var redAgent = Substitute.For<IAgent>();
            redAgent.PurpleTeam.Returns(false);

            BoardController sut = GetSut(board, 2);
            sut.Agents[0, 0] = blueAgent;
            sut.Agents[1, 1] = redAgent;
            sut.OrangeBase[0, 0] = true;

            // Act
            sut.CheckForRoundEnd();

            // Assert
            board.DidNotReceive().EndRound(Arg.Any<RoundOutcome>());
        }
        [Test]
        public void EndTurn_BlueTurn_ChangesTurnAndRefreshesMovementAndColoursButtonRed()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            BoardController sut = GetSut(board, 1);
            sut.PurpleTeamTurn = true;

            // Act
            sut.EndTurn();

            // Assert
            Assert.False(sut.PurpleTeamTurn, "Turn should change");
            board.Received().RefreshAgentMovements();
            board.Received().ColourEndTurnOrange();
        }

        [Test]
        public void EndTurn_RedTurn_ChangesTurnAndRefreshesMovementAndColoursButtonBlue()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            BoardController sut = GetSut(board, 1);
            sut.PurpleTeamTurn = false;

            // Act
            sut.EndTurn();

            // Assert
            Assert.True(sut.PurpleTeamTurn, "Turn should change");
            board.Received().RefreshAgentMovements();
            board.Received().ColourEndTurnPurple();
        }

        [Test]
        public void EndTurn_AgentIsSelectedWhenTurnEnds_DeselectsAgent()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var agent = Substitute.For<IAgent>();

            BoardController sut = GetSut(board, 1);
            sut.SelectedAgent = agent;

            // Act
            sut.EndTurn();

            // Assert
            Assert.Null(sut.SelectedAgent);
        }
    }
}
