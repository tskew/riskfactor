﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Logic;
using Assets.Scripts.Utility;
using NSubstitute;
using NUnit.Framework;

namespace Assets.Editor
{
    [TestFixture]
    public class GraphicsControllerTests
    {
        [Test]
        public void SpawnAllPurpleBase_NoSquaresArePurpleBase_DoesNotCallRender()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var purpleBase = new bool[1, 1];

            var sut = new GraphicsController(behaviour, 1);

            // Act
            sut.SpawnAllPurpleBase(purpleBase);

            // Assert
            behaviour.DidNotReceive().RenderPurpleBase(Arg.Any<int>(), Arg.Any<int>());
        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        public void SpawnAllPurpleBase_SomeSquaresArePupleBase_CallsRenderForEachPurpleBase(int baseX, int baseY)
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var purpleBase = new bool[2, 2];
            purpleBase[baseX, baseY] = true;

            var sut = new GraphicsController(behaviour, 2);

            // Act
            sut.SpawnAllPurpleBase(purpleBase);

            // Assert
            behaviour.Received(1).RenderPurpleBase(Arg.Any<int>(), Arg.Any<int>());
            behaviour.Received().RenderPurpleBase(baseX, baseY);
        }

        [Test]
        public void SpawnAllOrangeBase_NoSquaresAreOrangeBase_DoesNotCallRender()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var orangeBase = new bool[1, 1];

            var sut = new GraphicsController(behaviour, 1);

            // Act
            sut.SpawnAllOrangeBase(orangeBase);

            // Assert
            behaviour.DidNotReceive().RenderOrangeBase(Arg.Any<int>(), Arg.Any<int>());
        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        public void SpawnAllOrangeBase_SomeSquaresAreOrangeBase_CallsRenderForEachOrangeBase(int baseX, int baseY)
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var orangeBase = new bool[2, 2];
            orangeBase[baseX, baseY] = true;

            var sut = new GraphicsController(behaviour, 2);

            // Act
            sut.SpawnAllOrangeBase(orangeBase);

            // Assert
            behaviour.Received(1).RenderOrangeBase(Arg.Any<int>(), Arg.Any<int>());
            behaviour.Received().RenderOrangeBase(baseX, baseY);
        }

        [Test]
        public void SpawnWalls_NoSquaresAreWalls_DoesNotCallAnyRender()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[1, 1];

            var sut = new GraphicsController(behaviour, 1);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.DidNotReceive().RenderWall(Arg.Any<int>(), Arg.Any<int>());
        }

        [TestCase(0, 0)]
        [TestCase(0, 1)]
        [TestCase(1, 0)]
        [TestCase(1, 1)]
        public void SpawnWalls_OneSquareIsWall_CallsRenderWithThatCoordinate(int wallX, int wallY)
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[2, 2];
            walls[wallX, wallY] = true;

            var sut = new GraphicsController(behaviour, 2);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received(1).RenderWall(Arg.Any<int>(), Arg.Any<int>());
            behaviour.Received().RenderWall(wallX, wallY);
        }

        [Test]
        public void SpawnWalls_TwoAdjacentSquaresInARowAreWalls_CallsRenderTwoPieceWithLowestCoordinateAndHorizontalOrientation()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[2, 2];
            walls[0, 0] = true;
            walls[1, 0] = true;

            var sut = new GraphicsController(behaviour, 2);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderTwoPieceWall(0, 0, WallOrientation.Horizontal);
        }

        [Test]
        public void SpawnWalls_TwoAdjacentSquaresInAColumnAreWalls_CallsRenderTwoPieceWithLowestCoordinateAndVerticalOrientation()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[2, 2];
            walls[0, 0] = true;
            walls[0, 1] = true;

            var sut = new GraphicsController(behaviour, 2);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderTwoPieceWall(0, 0, WallOrientation.Vertical);
        }

        [Test]
        public void SpawnWalls_TwoAdjacentSquares_DoesNotRenderSingleWallForAnySquare()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[2, 2];
            walls[0, 0] = true;
            walls[1, 0] = true;

            var sut = new GraphicsController(behaviour, 2);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.DidNotReceive().RenderWall(0, 0);
            behaviour.DidNotReceive().RenderWall(1, 0);
        }

        [Test]
        public void SpawnWalls_ThreeAdjacentSquaresInARowAreWalls_CallsRenderThreePieceWithLowestCoordinateAndHorizontalOrientation()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[3, 3];
            walls[0, 0] = true;
            walls[1, 0] = true;
            walls[2, 0] = true;

            var sut = new GraphicsController(behaviour, 3);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderThreePieceWall(0, 0, WallOrientation.Horizontal);
        }

        [Test]
        public void SpawnWalls_ThreeAdjacentSquaresInAColumnAreWalls_CallsRenderThreePieceWithLowestCoordinateAndVerticalOrientation()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[3, 3];
            walls[0, 0] = true;
            walls[0, 1] = true;
            walls[0, 2] = true;

            var sut = new GraphicsController(behaviour, 3);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderThreePieceWall(0, 0, WallOrientation.Vertical);
        }

        [Test]
        public void SpawnWalls_SingleWallsSeparatedByASpace_RendersSingleWallsInAllSpaces()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[3, 3];
            walls[0, 0] = true;
            walls[2, 0] = true;
            walls[0, 2] = true;
            walls[2, 2] = true;

            var sut = new GraphicsController(behaviour, 3);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderWall(0, 0);
            behaviour.Received().RenderWall(2, 0);
            behaviour.Received().RenderWall(0, 2);
            behaviour.Received().RenderWall(2, 2);
            behaviour.DidNotReceive().RenderThreePieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
        }

        [Test]
        public void SpawnWalls_ThreeAdjacentSquares_DoesNotRenderOtherWallsInThoseSquares()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[3, 3];
            walls[0, 0] = true;
            walls[1, 0] = true;
            walls[2, 0] = true;

            var sut = new GraphicsController(behaviour, 3);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderThreePieceWall(0, 0, WallOrientation.Horizontal);

            behaviour.DidNotReceive().RenderTwoPieceWall(0, 0, Arg.Any<WallOrientation>());
            behaviour.DidNotReceive().RenderTwoPieceWall(1, 0, Arg.Any<WallOrientation>());

            behaviour.DidNotReceive().RenderWall(0, 0);
            behaviour.DidNotReceive().RenderWall(1, 0);
            behaviour.DidNotReceive().RenderWall(2, 0);
        }

        [Test]
        public void SpawnWalls_LBlockOfIntersectingWalls_RendersLongerWallsBeforeLookingForShorterWalls()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[3, 3];
            walls[1, 0] = true;
            walls[2, 0] = true;
            walls[2, 1] = true;
            walls[2, 2] = true;

            var sut = new GraphicsController(behaviour, 3);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderThreePieceWall(2, 0, WallOrientation.Vertical);
            behaviour.Received().RenderWall(1, 0);
            behaviour.Received(1).RenderWall(Arg.Any<int>(), Arg.Any<int>());

            behaviour.DidNotReceive().RenderTwoPieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
        }

        [Test]
        public void SpawnWalls_FourAdjacentSquares_CallsRenderFourPiece()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[4, 4];
            walls[0, 0] = true;
            walls[1, 0] = true;
            walls[2, 0] = true;
            walls[3, 0] = true;

            var sut = new GraphicsController(behaviour, 4);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderFourPieceWall(0, 0, WallOrientation.Horizontal);

            behaviour.DidNotReceive().RenderThreePieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
            behaviour.DidNotReceive().RenderTwoPieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
            behaviour.DidNotReceive().RenderWall(Arg.Any<int>(), Arg.Any<int>());
        }

        [Test]
        public void SpawnWalls_FiveAdjacentSquares_CallsRenderFivePiece()
        {
            // Arrange
            var behaviour = Substitute.For<IGraphicsBehaviour>();
            var walls = new bool[5, 5];
            walls[0, 0] = true;
            walls[1, 0] = true;
            walls[2, 0] = true;
            walls[3, 0] = true;
            walls[4, 0] = true;

            var sut = new GraphicsController(behaviour, 5);

            // Act
            sut.SpawnWalls(walls);

            // Assert
            behaviour.Received().RenderFivePieceWall(0, 0, WallOrientation.Horizontal);

            behaviour.DidNotReceive().RenderFourPieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
            behaviour.DidNotReceive().RenderThreePieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
            behaviour.DidNotReceive().RenderTwoPieceWall(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<WallOrientation>());
            behaviour.DidNotReceive().RenderWall(Arg.Any<int>(), Arg.Any<int>());
        }
    }
}
