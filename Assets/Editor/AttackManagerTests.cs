﻿using Assets.Scripts.Interfaces;
using Assets.Scripts.Logic;
using NSubstitute;
using NUnit.Framework;

namespace Assets.Editor
{
    [TestFixture]
    public class AttackManagerTests
    {
        [Test]
        public void MakeRangedAttack_AttackDoesNotHit_DefenderDoesNotRollArmour()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var attacker = GetAgent(0, 1);
            attacker.RollAttack().Returns(false);

            var defender = GetAgent(2, 3);

            AttackManager sut = GetSut(board);

            // Act
            sut.MakeRangedAttack(attacker, defender);

            // Assert
            defender.DidNotReceive().RollArmourBreak();
            board.DidNotReceive().DownAgent(Arg.Any<int>(), Arg.Any<int>());
        }

        private IAgent GetAgent(int x, int y)
        {
            var agent = Substitute.For<IAgent>();
            agent.CurrentX.Returns(x);
            agent.CurrentY.Returns(y);
            return agent;
        }

        private AttackManager GetSut(IBoardBehaviour board)
        {
            return new AttackManager(board);
        }

        [Test]
        public void MakeRangedAttack_AttackHitsButDoesNotBreakArmour_DefenderIsNotDowned()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var attacker = GetAgent(0, 1);
            attacker.RollAttack().Returns(true);

            var defender = GetAgent(2, 3);
            defender.RollArmourBreak().Returns(false);

            AttackManager sut = GetSut(board);

            // Act
            sut.MakeRangedAttack(attacker, defender);

            // Assert
            board.DidNotReceive().DownAgent(Arg.Any<int>(), Arg.Any<int>());
        }

        [Test]
        public void MakeRangedAttack_AttackHitsAndBreaksArmour_DefenderIsDowned()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var attacker = GetAgent(0, 1);
            attacker.RollAttack().Returns(true);

            var defender = GetAgent(2, 3);
            defender.RollArmourBreak().Returns(true);
            defender.HoldingFlag.Returns(false);

            AttackManager sut = GetSut(board);

            // Act
            sut.MakeRangedAttack(attacker, defender);

            // Assert
            board.Received().DownAgent(2, 3);
        }

        [Test]
        public void MakeMeleeAttacks_AttackerPassesMeleeRoll_DefenderIsDowned()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var attacker = GetAgent(0, 0);
            attacker.RollMelee().Returns(true);

            var defender = GetAgent(1, 1);
            defender.RollMelee().Returns(false);

            AttackManager sut = GetSut(board);

            // Act
            sut.MakeMeleeAttacks(attacker, defender);

            // Assert
            board.DidNotReceive().DownAgent(0, 0);
            board.Received().DownAgent(1, 1);
        }

        [Test]
        public void MakeMeleeAttacks_DefenderPassesMeleeRoll_AttackerIsDowned()
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var attacker = GetAgent(0, 0);
            attacker.RollMelee().Returns(false);

            var defender = GetAgent(1, 1);
            defender.RollMelee().Returns(true);

            AttackManager sut = GetSut(board);

            // Act
            sut.MakeMeleeAttacks(attacker, defender);

            // Assert
            board.Received().DownAgent(0, 0);
            board.DidNotReceive().DownAgent(1, 1);
        }

        [TestCase(0, 0, 1, 1)]
        [TestCase(1, 1, 0, 0)]
        [TestCase(0, 0, 0, 1)]
        [TestCase(0, 0, 1, 0)]
        public void MakeMeleeAttacks_BothAgentsPassMeleeRolls_AttackerAndDefenderAreDowned(int attackerX, int attackerY, int defenderX, int defenderY)
        {
            // Arrange
            var board = Substitute.For<IBoardBehaviour>();

            var attacker = GetAgent(attackerX, attackerY);
            attacker.RollMelee().Returns(true);

            var defender = GetAgent(defenderX, defenderY);
            defender.RollMelee().Returns(true);

            AttackManager sut = GetSut(board);

            // Act
            sut.MakeMeleeAttacks(attacker, defender);

            // Assert
            board.Received().DownAgent(attackerX, attackerY);
            board.Received().DownAgent(defenderX, defenderY);
        }
    }
}
