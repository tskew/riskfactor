﻿using NUnit.Framework;
using NSubstitute;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Logic;

namespace Assets.Editor
{
    [TestFixture]
    public class AgentControllerTests
    {
        [TestCase(1, 2)]
        [TestCase(2, 4)]
        [TestCase(3, 6)]
        public void RefreshMovement_Called_MovementIsSetToDoubleSpeedAndAgentHasNotAttacked(int speed, int movement)
        {
            // Arrange
            var agent = Substitute.For<IAgentBehaviour>();
            agent.Speed.Returns(speed);

            var sut = new AgentController(agent);
            sut.HasAttacked = true;

            // Act
            sut.RefreshMovement();

            // Assert
            Assert.AreEqual(movement, sut.RemainingMovement);
            Assert.False(sut.HasAttacked, "Attack should have been refreshed");
        }

        [Test]
        public void PossibleMoves_GridHasSpacesBeyondAgentsSpace_CanMoveTwiceAsFarAsSpeed()
        {
            // Arrange
            var gridSize = 17;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Speed.Returns(1);

            var sut = new AgentController(agent);
            sut.CurrentX = 3;
            sut.CurrentY = 3;
            sut.RemainingMovement = 2;

            // Act
            var result = sut.PossibleMoves(new IAgent[gridSize, gridSize], new bool[gridSize, gridSize]);

            // Assert
            Assert.False(result[3, 3], "Should not be able to move to own square");

            Assert.True(result[3, 4], "Should be able to move 1 space north");
            Assert.True(result[3, 5], "Should be able to move 2 spaces north");
            Assert.False(result[3, 6], "Should not be able to move 3 spaces north");

            Assert.True(result[4, 4], "Should be able to move 1 space north-east");
            Assert.True(result[5, 5], "Should be able to move 2 spaces north-east");
            Assert.False(result[6, 6], "Should not be able to move 3 spaces north-east");

            Assert.True(result[4, 3], "Should be able to move 1 space east");
            Assert.True(result[5, 3], "Should be able to move 2 spaces east");
            Assert.False(result[6, 3], "Should not be able to move 3 spaces east");

            Assert.True(result[4, 2], "Should be able to move 1 space south-east");
            Assert.True(result[5, 1], "Should be able to move 2 spaces south-east");
            Assert.False(result[6, 0], "Should not be able to move 3 spaces south-east");

            Assert.True(result[3, 2], "Should be able to move 1 space south");
            Assert.True(result[3, 1], "Should be able to move 2 spaces south");
            Assert.False(result[3, 0], "Should not be able to move 3 spaces south");

            Assert.True(result[2, 2], "Should be able to move 1 space south-west");
            Assert.True(result[1, 1], "Should be able to move 2 spaces south-west");
            Assert.False(result[0, 0], "Should not be able to move 3 spaces south-west");

            Assert.True(result[2, 3], "Should be able to move 1 space west");
            Assert.True(result[1, 3], "Should be able to move 2 spaces west");
            Assert.False(result[0, 3], "Should not be able to move 3 spaces west");

            Assert.True(result[2, 4], "Should be able to move 1 space north-west");
            Assert.True(result[1, 5], "Should be able to move 2 spaces north-west");
            Assert.False(result[0, 6], "Should not be able to move 3 spaces north-west");
        }

        [Test]
        public void PossibleMoves_AgentMovesWouldExtendOutsideGridBounds_HandlesInvalidMovement()
        {
            // Arrange
            var gridSize = 17;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Speed.Returns(1);

            var sut = new AgentController(agent);
            sut.CurrentX = 1;
            sut.CurrentY = 1;
            sut.RemainingMovement = 2;

            // Act
            var result = sut.PossibleMoves(new IAgent[gridSize, gridSize], new bool[gridSize, gridSize]);

            // Assert
            Assert.True(result[0, 0], "Should be able to move south-west");
            Assert.True(result[1, 0], "Should be able to move south");
            Assert.True(result[2, 0], "Should be able to move south-east");
            Assert.True(result[2, 1], "Should be able to move east");
            Assert.True(result[2, 2], "Should be able to move north-east");
            Assert.True(result[1, 2], "Should be able to move north");
            Assert.True(result[0, 2], "Should be able to move north-west");
            Assert.True(result[0, 1], "Should be able to move west");
        }

        [Test]
        public void PossibleMoves_AgentHasAlreadyTakenPartOfItsMovementThisTurn_CanMoveUpToTheRestOfMovementAllowance()
        {
            // Arrange
            var gridSize = 5;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Speed.Returns(1);

            var sut = new AgentController(agent);
            sut.CurrentX = 2;
            sut.CurrentY = 2;
            sut.RemainingMovement = 1;

            // Act
            var result = sut.PossibleMoves(new IAgent[gridSize, gridSize], new bool[gridSize, gridSize]);

            // Assert
            Assert.True(result[2, 3], "Should be able to move 1 space north");
            Assert.False(result[2, 4], "Should not be able to move 2 spaces north");

            Assert.True(result[3, 2], "Should be able to move 1 space east");
            Assert.False(result[4, 2], "Should not be able to move 2 spaces east");
        }

        [Test]
        public void PossibleMoves_PossibleSpaceIsAlreadyOccupied_CannotMoveToThatSquareOrBeyond()
        {
            // Arrange
            var gridSize = 3;
            var agent = Substitute.For<IAgentBehaviour>();
            var otherAgent = Substitute.For<IAgent>();

            var agents = new IAgent[gridSize, gridSize];
            agents[0, 1] = otherAgent;

            var sut = new AgentController(agent);
            sut.CurrentX = 0;
            sut.CurrentY = 0;
            sut.RemainingMovement = 2;

            // Act
            var result = sut.PossibleMoves(agents, new bool[gridSize, gridSize]);

            // Assert
            Assert.False(result[0, 1], "Should not be able to move into occupied space");
            Assert.False(result[0, 2], "Should not be able to move beyond occupied space");
        }

        [Test]
        public void PossibleMoves_PossibleSpaceIsImpassableTerrain_CannotMoveToThatSquareOrBeyond()
        {
            // Arrange
            var gridSize = 3;
            var agent = Substitute.For<IAgentBehaviour>();

            var terrain = new bool[gridSize, gridSize];
            terrain[0, 1] = true;

            var sut = new AgentController(agent);
            sut.CurrentX = 0;
            sut.CurrentY = 0;
            sut.RemainingMovement = 2;

            // Act
            var result = sut.PossibleMoves(new IAgent[gridSize, gridSize], terrain);

            // Assert
            Assert.False(result[0, 1], "Should not be able to move into impassable terrain");
            Assert.False(result[0, 2], "Should not be able to move beyond impassable terrain");
        }

        [TestCase(1, 1, 0, 0)]
        [TestCase(2, 1, 1, 1)]
        [TestCase(3, 0, 2, 1)]
        [TestCase(5, 2, 2, 3)]
        public void MoveTo_PassedCoordinatesWithinMovementRange_MovesToNewTileAndRemovesMovementAllowance(int beforeMovement, int xDestination, int yDestination, int remainingMovement)
        {
            // Arrange
            var agent = Substitute.For<IAgentBehaviour>();

            var sut = new AgentController(agent);
            sut.CurrentX = 0;
            sut.CurrentY = 0;
            sut.RemainingMovement = beforeMovement;

            // Act
            sut.MoveTo(xDestination, yDestination);

            // Assert
            Assert.AreEqual(xDestination, sut.CurrentX);
            Assert.AreEqual(yDestination, sut.CurrentY);
            Assert.AreEqual(remainingMovement, sut.RemainingMovement);
            agent.Received().SetTransformPosition(xDestination, yDestination);
        }

        [Test]
        public void PickupFlag_AgentHasRemainingMovement_AgentIsNowHoldingFlagAndHasNoMoreMovement()
        {
            // Arrange
            var agent = Substitute.For<IAgentBehaviour>();

            var sut = new AgentController(agent);
            sut.RemainingMovement = 1;
            sut.HoldingFlag = false;

            // Act
            sut.PickupFlag();

            // Asset
            Assert.True(sut.HoldingFlag, "Should be holding flag");
            Assert.AreEqual(0, sut.RemainingMovement);
        }

        [Test]
        public void PossibleAttacks_NoAgentsAreWithinAttackRange_AttacksPossibleUpToAttackRange()
        {
            // Arrange
            const int gridSize = 5;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Range.Returns(1);

            var enmy = Substitute.For<IAgent>();

            var agents = new IAgent[gridSize, gridSize] { { enmy, enmy, enmy, enmy, enmy },
                                                          { enmy, null, null, null, enmy },
                                                          { enmy, null, null, null, enmy },
                                                          { enmy, null, null, null, enmy },
                                                          { enmy, enmy, enmy, enmy, enmy } };

            var sut = new AgentController(agent);
            sut.CurrentX = 2;
            sut.CurrentY = 2;

            // Act
            var result = sut.PossibleRangedAttacks(agents, new bool[gridSize, gridSize]);

            // Assert
            Assert.True(result[1, 1]);
            Assert.True(result[1, 3]);
            Assert.True(result[3, 3]);
            Assert.True(result[3, 1]);
        }

        [Test]
        public void PossibleAttacks_AgentIsWithinRange_CanAttackThatSpace()
        {
            // Arrange
            const int gridSize = 7;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Range.Returns(3);

            var self = Substitute.For<IAgent>();

            var enmy = Substitute.For<IAgent>();

            var agents = new IAgent[gridSize, gridSize] { { enmy, enmy, enmy, enmy, enmy, enmy, enmy },
                                                          { enmy, null, null, null, null, null, enmy },
                                                          { enmy, null, null, null, null, null, enmy },
                                                          { enmy, null, null, self, null, null, enmy },
                                                          { enmy, null, null, null, null, null, enmy },
                                                          { enmy, null, null, null, null, null, enmy },
                                                          { enmy, enmy, enmy, enmy, enmy, enmy, enmy } };

            var sut = new AgentController(agent);
            sut.CurrentX = 3;
            sut.CurrentY = 3;

            // Act
            var result = sut.PossibleRangedAttacks(agents, new bool[gridSize, gridSize]);

            // Assert
            for (int i = 0; i < gridSize; i++)
            {
                Assert.True(result[i, 0], string.Format("Should be able to attack enemy at [{0}, 0]", i));
                Assert.True(result[i, gridSize - 1], string.Format("Should be able to attack enemy at [{0}, {1}]", i, gridSize - 1));

                Assert.True(result[0, i], string.Format("Should be able to attack enemy at [0, {0}]", i));
                Assert.True(result[gridSize - 1, i], string.Format("Should be able to attack enemy at [{0}, {1}]", gridSize - 1, i));
            }

            Assert.False(result[3, 3], "Should not be able to attack own square");
        }

        [Test]
        public void PossibleAttacks_AgentIsWithinRangeButObstacleIsBetweenAgents_CannotAttackThatSpace()
        {
            // Arrange
            const int gridSize = 3;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Range.Returns(2);

            var otherAgent = Substitute.For<IAgent>();

            var agents = new IAgent[gridSize, gridSize];
            agents[2, 0] = otherAgent;
            agents[0, 1] = otherAgent;
            agents[0, 2] = otherAgent;

            var terrain = new bool[gridSize, gridSize];
            terrain[1, 0] = true;

            var sut = new AgentController(agent);
            sut.CurrentX = 0;
            sut.CurrentY = 0;

            // Act
            var result = sut.PossibleRangedAttacks(agents, terrain);

            Assert.False(result[2, 0], "Should not be able to attack through terrian");
            Assert.True(result[0, 1]);
            Assert.False(result[0, 2], "Should not be able to attack through enemy agents");
        }

        [Test]
        public void PossibleAttacks_RangeExtendsOutsideBoundsOfGrid_HandlesOverflow()
        {
            // Arrange
            int gridSize = 17;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Range.Returns(2);

            var otherAgent = Substitute.For<IAgent>();

            var agents = new IAgent[gridSize, gridSize];
            agents[1, 1] = otherAgent;

            var sut = new AgentController(agent);
            sut.CurrentX = 0;
            sut.CurrentY = 0;

            // Act
            var result = sut.PossibleRangedAttacks(agents, new bool[gridSize, gridSize]);

            // Assert
            Assert.True(result[1, 1], "Should be able to attack north-east");
        }

        [Test]
        public void PossibelAttacks_AgentInRangeButHasAlreadyAttacked_CannotMakeAnyAttacks()
        {
            // Arrange
            int gridSize = 2;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Range.Returns(1);

            var otherAgent = Substitute.For<IAgent>();

            var agents = new IAgent[gridSize, gridSize];
            agents[1, 1] = otherAgent;

            var sut = new AgentController(agent);
            sut.CurrentX = 0;
            sut.CurrentY = 0;
            sut.HasAttacked = true;

            // Act
            var result = sut.PossibleRangedAttacks(agents, new bool[gridSize, gridSize]);

            // Assert
            Assert.False(result[1, 1], "Should not be able to attack after attacking");
        }

        [Test]
        public void ValidAttacks_AgentHasNoPossibleAttacks_ReturnsGridOfAllFalse()
        {
            // Arrange
            int gridSize = 17;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.PurpleTeam.Returns(true);

            var self = Substitute.For<IAgent>();
            self.PurpleTeam.Returns(true);

            var otherAgent = Substitute.For<IAgent>();
            otherAgent.PurpleTeam.Returns(false);

            var agents = new IAgent[gridSize, gridSize];
            agents[0, 0] = self;
            agents[1, 0] = otherAgent;
            agents[1, 1] = otherAgent;
            agents[0, 1] = otherAgent;

            var sut = new AgentController(agent);

            // Act
            var result = sut.ValidAttacks(new bool[gridSize, gridSize], agents);

            // Assert
            Assert.False(result[0, 0]);
            Assert.False(result[1, 0]);
            Assert.False(result[1, 1]);
            Assert.False(result[0, 1]);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidAttacks_AgentOfOppositeTeamIsInSquareOfPossibleAttack_ReturnsTrueForThatSquare(bool purpleTeam)
        {
            // Arrange
            int gridSize = 17;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.PurpleTeam.Returns(purpleTeam);

            var otherAgent = Substitute.For<IAgent>();
            otherAgent.PurpleTeam.Returns(!purpleTeam);

            var agents = new IAgent[gridSize, gridSize];
            agents[0, 1] = otherAgent;
            agents[1, 0] = otherAgent;

            var possibleAttacks = new bool[gridSize, gridSize];
            possibleAttacks[0, 0] = true;
            possibleAttacks[0, 1] = true;
            possibleAttacks[1, 0] = true;

            var sut = new AgentController(agent);

            // Act
            var result = sut.ValidAttacks(possibleAttacks, agents);

            // Assert
            Assert.False(result[0, 0]);
            Assert.True(possibleAttacks[0, 0]);
            Assert.True(result[0, 1]);
            Assert.False(possibleAttacks[0, 1]);
            Assert.True(result[1, 0]);
            Assert.False(possibleAttacks[0, 1]);
        }

        [Test]
        public void ValidAttacks_NoAgentOrAgentOfSameTeamIsInSquareOfPossibleAttack_ReturnsFalseForThatSquare()
        {
            // Arrange
            int gridSize = 17;

            var agent = Substitute.For<IAgentBehaviour>();
            agent.PurpleTeam.Returns(true);

            var ally = Substitute.For<IAgent>();
            ally.PurpleTeam.Returns(true);

            var agents = new IAgent[gridSize, gridSize];
            agents[0, 1] = ally;
            agents[1, 0] = ally;

            var possibleAttacks = new bool[gridSize, gridSize];
            possibleAttacks[0, 1] = true;
            possibleAttacks[1, 1] = true;
            possibleAttacks[1, 0] = true;

            var sut = new AgentController(agent);

            // Act
            var result = sut.ValidAttacks(possibleAttacks, agents);

            // Assert
            Assert.False(result[0, 1], "Same team should not be valid attack");
            Assert.False(result[1, 1], "Empty square should not be valid attack");
            Assert.False(result[1, 0], "Same team should not be valid attack");
        }

        [TestCase(2)]
        [TestCase(3)]
        public void RollMeleeAttack_RollIsEqualToOrLessThanStrengthStat_ReturnsTrue(int roll)
        {
            // Arrange
            var dice = GetDiceWhichRolls(roll);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Strength.Returns(3);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollMeleeAttack();

            // Assert
            Assert.True(result);
        }

        private IDiceRoller GetDiceWhichRolls(int roll)
        {
            var dice = Substitute.For<IDiceRoller>();
            dice.RollD6().Returns(roll);
            return dice;
        }

        [TestCase(4)]
        [TestCase(5)]
        public void RollMeleeAttack_RollIsGreaterThanStrengthStat_ReturnsFalse(int roll)
        {
            // Arrange
            var dice = GetDiceWhichRolls(roll);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Strength.Returns(2);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollMeleeAttack();

            // Assert
            Assert.False(result);
        }

        [Test]
        public void RollMeleeAttack_RollIs1_ReturnsTrue()
        {
            // Arrange
            var dice = GetDiceWhichRolls(1);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Strength.Returns(0);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollMeleeAttack();

            // Assert
            Assert.True(result);
        }

        [Test]
        public void RollMeleeAttack_RollIs6_ReturnsFalse()
        {
            // Arrange
            var dice = GetDiceWhichRolls(6);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Strength.Returns(7);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollMeleeAttack();

            // Assert
            Assert.False(result);
        }

        [Test]
        public void RollMeleeAttack_AgentHasMovementRemaining_RemainingMovementEndsAndMarksAsHavingAttacked()
        {
            // Arrange
            var agent = Substitute.For<IAgentBehaviour>();

            var sut = new AgentController(agent);
            sut.RemainingMovement = 1;

            // Act
            sut.RollMeleeAttack();

            // Assert
            Assert.AreEqual(0, sut.RemainingMovement);
            Assert.True(sut.HasAttacked, "Should have been marked as having attacked");
        }

        [TestCase(2)]
        [TestCase(3)]
        public void RollRangedAttack_RollIsEqualToOrLessThanMarksmanshipStat_ReturnsTrue(int roll)
        {
            // Arrange
            var dice = GetDiceWhichRolls(roll);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Marksmanship.Returns(3);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollRangedAttack();

            // Assert
            Assert.True(result);
        }

        [TestCase(4)]
        [TestCase(5)]
        public void RollRangedAttack_RollIsGreaterThanMarksmanshipStat_ReturnsFalse(int roll)
        {
            // Arrange
            var dice = GetDiceWhichRolls(roll);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Marksmanship.Returns(3);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollRangedAttack();

            // Assert
            Assert.False(result);
        }

        [Test]
        public void RollRangedAttack_RollIs1_ReturnsTrue()
        {
            // Arrange
            var dice = GetDiceWhichRolls(1);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Marksmanship.Returns(0);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollRangedAttack();

            // Assert
            Assert.True(result);
        }

        [Test]
        public void RollRangedAttack_RollIs6_ReturnsFalse()
        {
            // Arrange
            var dice = GetDiceWhichRolls(6);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Marksmanship.Returns(7);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollRangedAttack();

            // Assert
            Assert.False(result);
        }

        [Test]
        public void RollRangedAttack_AgentHasMovementRemaining_RemainingMovementEndsAndMarksAsHavingAttacked()
        {
            // Arrange
            var agent = Substitute.For<IAgentBehaviour>();

            var sut = new AgentController(agent);
            sut.RemainingMovement = 1;

            // Act
            sut.RollRangedAttack();

            // Assert
            Assert.AreEqual(0, sut.RemainingMovement);
            Assert.True(sut.HasAttacked, "Should have been marked as having attacked");
        }

        [TestCase(4)]
        [TestCase(5)]
        public void RollArmourBreak_RollIsGreaterThanArmour_ReturnsTrue(int roll)
        {
            // Arrange
            var dice = GetDiceWhichRolls(roll);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Armour.Returns(3);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollArmourBreak();

            // Assert
            Assert.True(result);
        }

        [TestCase(2)]
        [TestCase(3)]
        public void RollArmourBreak_RollIsLessThanOrEqualToArmour_ReturnsFalse(int roll)
        {
            // Arrange
            var dice = GetDiceWhichRolls(roll);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Armour.Returns(3);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollArmourBreak();

            // Assert
            Assert.False(result);
        }

        [Test]
        public void RollArmourBreak_RollIs6_ReturnsTrue()
        {
            // Arrange
            var dice = GetDiceWhichRolls(6);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Armour.Returns(7);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollArmourBreak();

            // Assert
            Assert.True(result);
        }

        [Test]
        public void RollArmourBreak_RollIs1_ReturnsFalse()
        {
            // Arrange
            var dice = GetDiceWhichRolls(1);

            var agent = Substitute.For<IAgentBehaviour>();
            agent.Dice.Returns(dice);
            agent.Armour.Returns(0);

            var sut = new AgentController(agent);

            // Act
            var result = sut.RollArmourBreak();

            // Assert
            Assert.False(result);
        }
    }
}
